package errors;

/**
 * кастомное исключение для обработки некорректного ввода значений
 * переопределяет класс Exception, пола и методы делают то же самое
 * @author Max Nigmatulin
 * @version 1.0
 */
public class WrongInputException extends Exception {
    private String message;

    public WrongInputException(String msg) {
        this.message = msg;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}

