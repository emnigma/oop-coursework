package logic;

import javax.persistence.*;
import java.util.List;

import errors.WrongInputException;
import school_database_classes.*;

/**
 * класс для добавления сущеностей в базув
 * @author Max Nigmatulin
 * @version 1.0
 */
public class EntityAdder {

    static EntityManagerFactory emf = Persistence.createEntityManagerFactory("default");

    /**
     *
     * @param class_number - номер класса
     * @param class_letter - буква класса
     * @throws WrongInputException - если введены некорректные данные или класс существует
     */
    public void addClass(int class_number, String class_letter) throws WrongInputException {
        if (class_letter.isEmpty() || class_letter.length() > 1) {
            throw new WrongInputException("Enter valid letter");
        }

        EntityManager em = emf.createEntityManager();

        //checking for existing class
        em.getTransaction().begin();

        //finding class by class number and letter and check if it exists in db
        List<ClazzEntity> class_in_database = em.createQuery("SELECT c FROM ClazzEntity c WHERE c.classNumber = '" + class_number + "' AND c.classLetter = '" + class_letter + "'").getResultList();
        em.getTransaction().commit();

        if (class_in_database.size() != 0) {
            throw new WrongInputException("This class already exists!");
        }
        //open transaction
        em.getTransaction().begin();

        //creating entity to pass to database
        ClazzEntity new_class = new ClazzEntity();

        //setting parameters
        new_class.setClassNumber(class_number);
        new_class.setClassLetter(class_letter);

        //committing transaction
        em.persist(new_class);
        em.getTransaction().commit();
    }

    /**
     * Класс для добавления учителя в базу
     * @param surname - фамилия
     * @param name - имя
     * @param patronymic - отчество
     * @param comment - комментарий
     * @throws WrongInputException - если введены некорректные данные
     */
    public void addTeacher(String surname, String name, String patronymic, String comment) throws WrongInputException {
        if (surname.isEmpty() || name.isEmpty()) {
            throw new WrongInputException("Enter valid credentials");
        }

        EntityManager em = emf.createEntityManager();

        //open transaction
        em.getTransaction().begin();

        //creating entity to pass to database
        TeacherEntity teacher = new TeacherEntity();

        //setting parameters
        teacher.setFirstname(name);
        teacher.setSurname(surname);
        teacher.setPatronymic(patronymic);
        teacher.setComment(comment);

        //committing transaction
        em.persist(teacher);
        em.getTransaction().commit();

        //printing logs
    }


    /**
     *
     * @param class_number - номер класса
     * @param class_letter - буква класса
     * @param surname - фамилия
     * @param name - имя
     * @param patronymic - отчество
     * @param comment - комментарий
     * @throws WrongInputException - если введены некорректные данные
     */
    public void addStudent(int class_number, String class_letter, String surname, String name, String patronymic, String comment) throws WrongInputException {
        EntityManager em = emf.createEntityManager();
        //open transaction
        em.getTransaction().begin();

        //finding class by class number and letter and check if it exists in db
        List<ClazzEntity> class_in_database = em.createQuery("SELECT c FROM ClazzEntity c WHERE c.classNumber = '" + class_number + "' AND c.classLetter = '" + class_letter + "'").getResultList();
        em.getTransaction().commit();

        if (class_letter.isEmpty() || surname.isEmpty() || name.isEmpty()) {
            throw new WrongInputException("Enter valid credentials");
        }

        if (!class_in_database.isEmpty()) {
            em.getTransaction().begin();
            em.persist(class_in_database.get(0));

            //creating entity to pass to database
            StudentEntity student = new StudentEntity();

            //setting parameters
            student.setFirstname(name);
            student.setSurname(surname);
            student.setPatronymic(patronymic);
            student.setComment(comment);
            student.setClazzByClassId(class_in_database.get(0));

            //committing transaction
            em.persist(student);
            em.getTransaction().commit();
        }
        else {
            throw new WrongInputException("There are no such class");
        }
    }
}
