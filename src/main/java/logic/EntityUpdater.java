package logic;

import javax.persistence.*;
import school_database_classes.*;
import errors.WrongInputException;

import java.util.List;

/**
 * класс для обновления сущностей
 * @author Max Nigmatulin
 * @version 1.0
 */
public class EntityUpdater {

    static EntityManagerFactory emf = Persistence.createEntityManagerFactory("default");

    /**
     *
     * @param entity_index - индекс класса
     * @param class_number - новый номер класса
     * @param class_letter - новая буква класса
     * @throws WrongInputException - если введены некорректные данные или класс существует
     */
    public void updateClass(int entity_index, int class_number, String class_letter) throws WrongInputException {
        if (class_letter.isEmpty() || class_letter.length() > 1) {
            throw new WrongInputException("Enter valid letter");
        }

        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        List<ClazzEntity> class_in_database = em.createQuery("SELECT c FROM ClazzEntity c WHERE c.classNumber = '" + class_number + "' AND c.classLetter = '" + class_letter + "'").getResultList();
        em.getTransaction().commit();

        if (!class_in_database.isEmpty()) {
            throw new WrongInputException("This class already exists");
        }

        em.getTransaction().begin();
        ClazzEntity class_to_update = em.find(ClazzEntity.class, entity_index);

        class_to_update.setClassLetter(class_letter);
        class_to_update.setClassNumber(class_number);

        em.getTransaction().commit();
    }

    /**
     *
     * @param entity_index - индекс в базе
     * @param surname - новая фамилия
     * @param firstname - новое имя
     * @param patronymic новое отчество
     * @param comment - новый комментарий
     * @throws WrongInputException - если введены некорректные данные
     */
    public void updateTeacher(
            int entity_index,
            String surname,
            String firstname,
            String patronymic,
            String comment) throws WrongInputException {

        if (surname.isEmpty() || firstname.isEmpty()) {
            throw new WrongInputException("Enter valid credentials");
        }

        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        TeacherEntity teacher_to_update = em.find(TeacherEntity.class, entity_index);

        teacher_to_update.setSurname(surname);
        teacher_to_update.setFirstname(firstname);
        teacher_to_update.setPatronymic(patronymic);
        teacher_to_update.setComment(comment);

        em.getTransaction().commit();
    }

    /**
     *
     * @param entity_index - индекс в базе
     * @param class_number - новый номер класса
     * @param class_letter - новая буква класса
     * @param surname - новая фамилия
     * @param firstname - новое имя
     * @param patronymic - новое отчество
     * @param comment
     * @throws WrongInputException - если введены некорректные данные или класс существует
     */
    public void updateStudent(
            int entity_index,
            int class_number,
            String class_letter,
            String surname,
            String firstname,
            String patronymic,
            String comment) throws WrongInputException {

        if (class_letter.isEmpty() || surname.isEmpty() || firstname.isEmpty()) {
            throw new WrongInputException("Enter valid credentials");
        }

        EntityManager em = emf.createEntityManager();

        //finding class by class number and letter and check if it exists in db
        em.getTransaction().begin();
        List<ClazzEntity> class_in_database = em.createQuery("SELECT c FROM ClazzEntity c WHERE c.classNumber = '" + class_number + "' AND c.classLetter = '" + class_letter + "'").getResultList();
        em.getTransaction().commit();

        if (!class_in_database.isEmpty()) {
            em.getTransaction().begin();

            StudentEntity student_to_update = em.find(StudentEntity.class, entity_index);

            student_to_update.setFirstname(firstname);
            student_to_update.setSurname(surname);
            student_to_update.setPatronymic(patronymic);
            student_to_update.setComment(comment);
            student_to_update.setClazzByClassId(class_in_database.get(0));

            //committing transaction
            em.getTransaction().commit();
        }
        else {
            throw new WrongInputException("There are no such class");
        }
    }
}
