package logic;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import javax.swing.table.DefaultTableModel;
import java.io.FileOutputStream;
import java.util.Vector;
import java.util.stream.Stream;

/**
 * класс для создания PDF-файлов
 */
public class PDFCreator {

    /**
     *
     * @param filename - имя файла
     * @param headers - список заголовков
     * @param table_model - модель с данными
     * @throws Exception - если возникла любая ошибка
     */
    public static void createSamplePDF(String filename, String[] headers, DefaultTableModel table_model) throws Exception{
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(filename));

        document.open();

        PdfPTable table = new PdfPTable(headers.length);
        addTableHeader(table, headers);
        addRows(table, table_model);

        document.add(table);
        document.close();
    }

    /**
     * функция добавления всех данных в PDFtable из DefaultTableModel
     * @param table - куда добавить
     * @param table_model - что добавить
     */
    private static void addRows(PdfPTable table, DefaultTableModel table_model) {
        Vector data = table_model.getDataVector();
        for (int i = 0; i < table_model.getRowCount(); i++) {
            for (int j = 0; j < table_model.getColumnCount(); j++) {
                table.addCell(((Vector)data.elementAt(i)).elementAt(j).toString());
            }
        }
    }

    /**
     * функция добавления заголовка таблицы
     * @param table - таблица для добавления заголовка
     * @param headers - заголовок
     */
    private static void addTableHeader(PdfPTable table, String[] headers) {
        Stream.of(headers)
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }
}
