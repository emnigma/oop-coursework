package logic;

import school_database_classes.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.table.DefaultTableModel;

import java.util.ArrayList;
import java.util.List;

/**
 * класс для обновления визуального представления таблицы
 * @author Max Nigmatulin
 * @version 1.0
 */
public class TableModelUpdater {

    static EntityManagerFactory emf = Persistence.createEntityManagerFactory("default");

    public static String[] columns;
    public static DefaultTableModel model;

    public void updateClass() {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();

        List<ClazzEntity> classes_list = em.createQuery("SELECT c FROM ClazzEntity c ORDER BY c.classNumber, c.classLetter").getResultList();
        em.getTransaction().commit();

        columns = new String[] {"class_id", "class name"};

        model = new DefaultTableModel(columns, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        for (ClazzEntity c: classes_list) {
            String[] row = {
                    Integer.toString(c.getClassId()),
                    c.getClassNumber() + " \"" + c.getClassLetter() + "\""
            };
            model.addRow(row);
        }

        app_GUI.table_model = model;
    }

    public void updateTeacher() {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        List<TeacherEntity> teachers_list = em.createQuery("SELECT t FROM TeacherEntity t ORDER BY t.surname, t.firstname, t.patronymic").getResultList();
        em.getTransaction().commit();

        columns = new String[] {"teacher_id", "surname", "firstname", "patronymic", "comment"};

        model = new DefaultTableModel(columns, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        for (TeacherEntity t: teachers_list) {
            String[] row = {
                    Integer.toString(t.getTeacherId()),
                    t.getSurname(),
                    t.getFirstname(),
                    t.getPatronymic(),
                    t.getComment()
            };
            model.addRow(row);
        }

        app_GUI.table_model = model;
    }

    public void updateStudent() {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        List<StudentEntity> students_list = em.createQuery("SELECT s FROM StudentEntity s ORDER BY s.clazzByClassId.classId, s.clazzByClassId.classLetter, s.surname, s.firstname, s.patronymic").getResultList();
        em.getTransaction().commit();

        columns = new String[] {"student id", "surname", "firstname", "patronymic", "class", "comment"};

        model = new DefaultTableModel(columns, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        for (StudentEntity s: students_list) {
            String[] row = {
                    Integer.toString(s.getStudentId()),
                    s.getSurname(),
                    s.getFirstname(),
                    s.getPatronymic(),
                    s.getClazzByClassId().getClassNumber() + " \"" + s.getClazzByClassId().getClassLetter() + "\"",
                    s.getComment()
            };
            model.addRow(row);
        }

        app_GUI.table_model = model;
    }

    public void updateFGraders() {

        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        List obj_list = em.createQuery(
                "SELECT s.studentId, s.surname, s.firstname, s.clazzByClassId.classNumber, s.clazzByClassId.classLetter, avg(g.value) as a_v FROM StudentEntity s join GradeEntity g on s.studentId = g.studentByStudentId.studentId GROUP BY s.studentId having avg(g.value) < 3.5 order by s.surname, s.firstname"
        ).getResultList();
        em.getTransaction().commit();

        columns = new String[] {"id", "surname", "firstname", "class", "avg"};

        model = new DefaultTableModel(columns, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        for (Object s: obj_list) {
            ArrayList<String> student_list = new ArrayList<>();
            for (int i = 0; i < 6; i++) {
                student_list.add(((Object[])s)[i].toString());
            }
            String[] row = {
                    student_list.get(0), //id
                    student_list.get(1), //surname
                    student_list.get(2), //firstname
                    student_list.get(3) + " \"" + student_list.get(4) + "\"", //class
                    student_list.get(5) //average
            };
            model.addRow(row);
        }

        app_GUI.table_model = model;
    }

    public void updateAGraders() {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        List obj_list = em.createQuery(
                "SELECT s.studentId, s.surname, s.firstname, s.clazzByClassId.classNumber, s.clazzByClassId.classLetter, avg(g.value) as a_v FROM StudentEntity s join GradeEntity g on s.studentId = g.studentByStudentId.studentId GROUP BY s.studentId having avg(g.value) > 4.5 order by s.surname, s.firstname"
        ).getResultList();
        em.getTransaction().commit();

        columns = new String[] {"id", "surname", "firstname", "class", "avg"};

        model = new DefaultTableModel(columns, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        for (Object s: obj_list) {
            ArrayList<String> student_list = new ArrayList<>();
            for (int i = 0; i < 6; i++) {
                student_list.add(((Object[])s)[i].toString());
            }
            String[] row = {
                    student_list.get(0), //id
                    student_list.get(1), //surname
                    student_list.get(2), //firstname
                    student_list.get(3) + " \"" + student_list.get(4) + "\"", //class
                    student_list.get(5) //average
            };
            model.addRow(row);
        }

        app_GUI.table_model = model;
    }

    /**
     * выводит на экран всех учителей и соответствующие им классы
     */
    public void updateHostAll() {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        List obj_list = em.createQuery(
                "SELECT t.teacherId, t.surname, t.firstname, h.clazzByClassId.classNumber, h.clazzByClassId.classLetter FROM TeacherEntity t join HostedClassEntity h on t.teacherId = h.teacherByTeacherId.teacherId order by t.surname, t.firstname"
        ).getResultList();
        em.getTransaction().commit();

        columns = new String[] {"id", "surname", "firstname", "class"};

        model = new DefaultTableModel(columns, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        for (Object s: obj_list) {
            ArrayList<String> hosts_list = new ArrayList<>();
            for (int i = 0; i < 5; i++) {
                hosts_list.add(((Object[])s)[i].toString());
            }
            String[] row = {
                    hosts_list.get(0), //id
                    hosts_list.get(1), //surname
                    hosts_list.get(2), //firstname
                    hosts_list.get(3) + " \"" + hosts_list.get(4) + "\"", //class
            };
            model.addRow(row);
        }

        app_GUI.table_model = model;
    }

    /**
     * выводит выбранного учителя и соответствующие ему классы
     * @param teacher_id - индекс учителя
     */
    public void updateHost(int teacher_id) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        List obj_list = em.createQuery(
                "SELECT t.teacherId, t.surname, t.firstname, h.clazzByClassId.classNumber, h.clazzByClassId.classLetter FROM TeacherEntity t join HostedClassEntity h on t.teacherId = h.teacherByTeacherId.teacherId where t.teacherId = " + teacher_id + " order by t.surname, t.firstname"
        ).getResultList();
        em.getTransaction().commit();

        String[] columns = {"id", "surname", "firstname", "class"};

        DefaultTableModel model = new DefaultTableModel(columns, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        for (Object s: obj_list) {
            ArrayList<String> hosts_list = new ArrayList<>();
            for (int i = 0; i < 5; i++) {
                hosts_list.add(((Object[])s)[i].toString());
            }
            String[] row = {
                    hosts_list.get(0), //id
                    hosts_list.get(1), //surname
                    hosts_list.get(2), //firstname
                    hosts_list.get(3) + " \"" + hosts_list.get(4) + "\"", //class
            };
            model.addRow(row);
        }

        app_GUI.table_model = model;
    }

    /**
     * выводит выбранного учителя и соответствующие ему предметы
     * @param teacher_id - индекс учителя
     */
    public void updateTeachesSubject(int teacher_id) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        List obj_list = em.createQuery(
                "SELECT t.teacherId, t.surname, t.firstname, c.subjectBySubjectId.name FROM TeacherEntity t join CompetenceEntity c on t.teacherId = c.teacherByTeacherId.teacherId where t.teacherId = " + Integer.toString(teacher_id) + " order by t.surname, t.firstname"
        ).getResultList();
        em.getTransaction().commit();

        String[] columns = {"id", "surname", "firstname", "subject"};

        DefaultTableModel model = new DefaultTableModel(columns, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        for (Object s: obj_list) {
            ArrayList<String> teaches_list = new ArrayList<>();
            for (int i = 0; i < 4; i++) {
                teaches_list.add(((Object[])s)[i].toString());
            }
            String[] row = {
                    teaches_list.get(0), //id
                    teaches_list.get(1), //surname
                    teaches_list.get(2), //firstname
                    teaches_list.get(3), //subject name
            };
            model.addRow(row);
        }

        app_GUI.table_model = model;
    }

    /**
     * выводит на экран всех учителей и соответствующие им предметы
     */
    public void updateTeachesSubjectAll() {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        List obj_list = em.createQuery(
                "SELECT t.teacherId, t.surname, t.firstname, c.subjectBySubjectId.name FROM TeacherEntity t join CompetenceEntity c on t.teacherId = c.teacherByTeacherId.teacherId order by t.surname, t.firstname"
        ).getResultList();
        em.getTransaction().commit();

        String[] columns = {"id", "surname", "firstname", "subject"};

        DefaultTableModel model = new DefaultTableModel(columns, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        for (Object s: obj_list) {
            ArrayList<String> teaches_list = new ArrayList<>();
            for (int i = 0; i < 4; i++) {
                teaches_list.add(((Object[])s)[i].toString());
            }
            String[] row = {
                    teaches_list.get(0), //id
                    teaches_list.get(1), //surname
                    teaches_list.get(2), //firstname
                    teaches_list.get(3), //subject name
            };
            model.addRow(row);
        }

        app_GUI.table_model = model;
    }

    /**
     * функция для формирования отчета по учащимся в классах
     * @return таблицу для формирования отчета по студентам
     */
    public DefaultTableModel createReportTableForStudentsCount() {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        List resultList = em.createQuery("select c.classId, c.classNumber, c.classLetter, count(s.studentId) as cnt from ClazzEntity c join StudentEntity s on s.clazzByClassId.classId = c.classId group by c.classId").getResultList();
        em.getTransaction().commit();

        DefaultTableModel model = new DefaultTableModel(new String[] {"class id", "class", "student count"}, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        for (Object s: resultList) {
            ArrayList<String> class_list = new ArrayList<>();
            for (int i = 0; i < 4; i++) {
                class_list.add(((Object[])s)[i].toString());
            }
            String[] row = {
                    class_list.get(0), //id
                    class_list.get(1) + " \"" + class_list.get(2) + "\"", //class
                    class_list.get(3) //count for each class

            };
            model.addRow(row);
        }

        String total_students = Integer.toString(app_GUI.count_students_total());

        model.addRow(new String[] {"", "RESULT:", total_students});

        return model;
    }
}
