package logic;

import school_database_classes.ClazzEntity;
import school_database_classes.StudentEntity;
import school_database_classes.TeacherEntity;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

import errors.WrongInputException;

/**
 * класс с интерфейсом и привязанными функциями
 * @author Max Nigmatulin
 * @version 1.0
 */
public class app_GUI extends JDialog {
    private JPanel contentPane;
    private JTable queryResultsTable;
    private JPanel tablePanel;
    private JPanel inputPanel;
    private JTabbedPane classesTabbedPane;
    private JTextField teacherSurnameTextField;
    private JTextField studentSurnameTextField;
    private JTextField classNumberTextField;
    private JTextField classLetterTextField;
    private JButton deleteButton;
    private JPanel classPanel;
    private JPanel teacherPanel;
    private JPanel studentPanel;
    private JScrollPane headers;
    private JTextField teacherFirstnameTextField;
    private JTextField teacherPatronymicTextField;
    private JTextField teacherCommentTextField;
    private JTextField studentFirstnameTextField;
    private JTextField studentPatronymicTextField;
    private JTextField studentCommentTextField;
    private JButton applyChangesButton;
    private JButton clearSelectButton;
    private JTextField studentClassNumberTextField;
    private JTextField studentClassLetterTextField;
    private JButton fGradersButton;
    private JButton aGradersButton;
    private JPanel statisticsPanel;
    private JButton hostsButton;
    private JLabel studentsCountTextField;
    private JLabel studentsCountDescription;
    private JButton teachesSubjectsButton;
    private JButton exportFGradersButton;
    private JButton exportAGradersButton;
    private JButton exportStudentsCountButton;

    /**
     * создает модель для отображения ее данных в таблицу
     */
    public static DefaultTableModel table_model;
    static EntityManagerFactory emf = Persistence.createEntityManagerFactory("default");

    TableModelUpdater tableModelUpdater = new TableModelUpdater();
    EntityUpdater entityUpdater = new EntityUpdater();
    EntityAdder entityAdder = new EntityAdder();

    /**
     *
     * @return количество студентов в базе данных
     */
    public static int count_students_total() {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        List resultList = em.createQuery("select s.studentId from StudentEntity s").getResultList();
        em.getTransaction().commit();

        return(resultList.size());
    }

    /**
     * возвращает таблицу к виду, соответствующему вкладке
     */
    public void clear_GUI() {
        applyChangesButton.setText("Add entity");
        classLetterTextField.setText("");
        classNumberTextField.setText("");

        teacherSurnameTextField.setText("");
        teacherFirstnameTextField.setText("");
        teacherPatronymicTextField.setText("");
        teacherCommentTextField.setText("");

        studentSurnameTextField.setText("");
        studentFirstnameTextField.setText("");
        studentPatronymicTextField.setText("");
        studentCommentTextField.setText("");
        studentClassLetterTextField.setText("");
        studentClassNumberTextField.setText("");
        queryResultsTable.clearSelection();

        studentsCountTextField.setText(Integer.toString(count_students_total()));

        int chosen_entity_index = classesTabbedPane.getSelectedIndex();
        String chosen_entity_name = classesTabbedPane.getTitleAt(chosen_entity_index);

        switch (chosen_entity_name) {
            case "class":
                tableModelUpdater.updateClass();
                queryResultsTable.setModel(table_model);
                break;

            case "teacher":
                tableModelUpdater.updateTeacher();
                queryResultsTable.setModel(table_model);
                break;

            case "student":
                tableModelUpdater.updateStudent();
                queryResultsTable.setModel(table_model);
                break;

        }
    }

    public app_GUI() {
        clear_GUI();
        tableModelUpdater.updateClass();
        queryResultsTable.setModel(table_model);

        setContentPane(contentPane);
        setModal(true);

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        classesTabbedPane.addChangeListener(
                new ChangeListener() {
                    @Override
                    /**
                     * по смене вкладки меняет таблицу на выбранную во вкладке
                     */
                    public void stateChanged(ChangeEvent e) {
                        int chosen_entity_index = classesTabbedPane.getSelectedIndex();
                        String chosen_entity_name = classesTabbedPane.getTitleAt(chosen_entity_index);
                        clear_GUI();

                        switch (chosen_entity_name) {
                            case "class":
                                tableModelUpdater.updateClass();
                                break;
                            case "teacher":
                                tableModelUpdater.updateTeacher();
                                break;
                            case "student":
                                tableModelUpdater.updateStudent();
                                break;
                        }

                        queryResultsTable.setModel(table_model);
                    }
                }
        );

        // на два клика значение из таблицы записывается в текстовые поля
        queryResultsTable.addMouseListener(
                new MouseAdapter() {
                    @Override
                    /**
                     * по клику мыши сущность записывается в текстовые поля справа
                     */
                    public void mouseClicked(MouseEvent e) {
                            applyChangesButton.setText("Update entity");
                            int chosen_entity_index = classesTabbedPane.getSelectedIndex();
                            String chosen_entity_name = classesTabbedPane.getTitleAt(chosen_entity_index);

                            switch (chosen_entity_name) {
                                case "class":
                                    String fused_class = table_model.getValueAt(queryResultsTable.getSelectedRow(), 1).toString();
                                    String class_number = fused_class.split(" ")[0];
                                    String class_letter = fused_class.split(" ")[1].replace("\"", "");
                                    classNumberTextField.setText(
                                            //get class number
                                            class_number
                                    );
                                    classLetterTextField.setText(
                                            //get class letter
                                            class_letter
                                    );
                                    EntityManager em = emf.createEntityManager();
                                    em.getTransaction().begin();
                                    List resultList = em.createQuery("select s.studentId from StudentEntity s where s.clazzByClassId.classLetter = '" + class_letter + "' and s.clazzByClassId.classNumber = '" + class_number + "'").getResultList();
                                    em.getTransaction().commit();
                                    studentsCountTextField.setText(Integer.toString(resultList.size()));

                                    break;

                                case "teacher":
                                    teacherSurnameTextField.setText(
                                            table_model.getValueAt(queryResultsTable.getSelectedRow(), 1).toString()
                                    );
                                    teacherFirstnameTextField.setText(
                                            table_model.getValueAt(queryResultsTable.getSelectedRow(), 2).toString()

                                    );
                                    teacherPatronymicTextField.setText(
                                            table_model.getValueAt(queryResultsTable.getSelectedRow(), 3).toString()
                                    );
                                    try {
                                        teacherCommentTextField.setText(
                                                table_model.getValueAt(queryResultsTable.getSelectedRow(), 4).toString()
                                        );
                                    }
                                    catch (NullPointerException nullPointerException) {
                                        System.out.println("No comment on this field");
                                    }
                                    break;
                                case "student":
                                    studentSurnameTextField.setText(
                                            table_model.getValueAt(queryResultsTable.getSelectedRow(), 1).toString()
                                    );
                                    studentFirstnameTextField.setText(
                                            table_model.getValueAt(queryResultsTable.getSelectedRow(), 2).toString()
                                    );
                                    studentPatronymicTextField.setText(
                                            table_model.getValueAt(queryResultsTable.getSelectedRow(), 3).toString()
                                    );
                                    //parsing clazz from table and passing its parts to text fields
                                    String student_fused_class = table_model.getValueAt(queryResultsTable.getSelectedRow(), 4).toString();
                                    String student_class_number = student_fused_class.split(" ")[0];
                                    String student_class_letter = student_fused_class.split(" ")[1].replace("\"", "");
                                    studentClassLetterTextField.setText(
                                            student_class_letter
                                    );
                                    studentClassNumberTextField.setText(
                                            student_class_number
                                    );
                                    try {
                                        studentCommentTextField.setText(
                                                table_model.getValueAt(queryResultsTable.getSelectedRow(), 5).toString()
                                        );
                                    }
                                    catch (NullPointerException nullPointerException) {
                                        System.out.println("No comment on this field");
                                    }
                                    break;
                            }
                        }
                }
        );

        deleteButton.addActionListener(
                new ActionListener() {
                    @Override
                    /**
                     * удаляет сущность
                     */
                    public void actionPerformed(ActionEvent e) {

                        EntityManager em = emf.createEntityManager();
                        em.getTransaction().begin();

                        int chosen_entity_index = classesTabbedPane.getSelectedIndex();
                        String chosen_entity_name = classesTabbedPane.getTitleAt(chosen_entity_index);

                        //получаем индекс выбранного ряда (вне зависимости от таблицы)
                        int chosen_row_index = queryResultsTable.getSelectedRow();

                        int input = JOptionPane.showConfirmDialog(contentPane, "Are you sure?", "", JOptionPane.OK_CANCEL_OPTION);
                        if (input != 0) {
                            return;
                        }

                        try {
                            switch (chosen_entity_name) {
                                case "class":
                                    ClazzEntity chosen_class = em.find(
                                            ClazzEntity.class,
                                            Integer.parseInt(queryResultsTable.getValueAt(chosen_row_index, 0).toString())
                                    );
                                    em.remove(chosen_class);
                                    em.getTransaction().commit();
                                    tableModelUpdater.updateClass();
                                    break;
                                case "teacher":
                                    TeacherEntity chosen_teacher = em.find(
                                            TeacherEntity.class,
                                            Integer.parseInt(queryResultsTable.getValueAt(chosen_row_index, 0).toString())
                                    );
                                    em.remove(chosen_teacher);
                                    em.getTransaction().commit();
                                    tableModelUpdater.updateTeacher();
                                    break;
                                case "student":
                                    StudentEntity chosen_student = em.find(
                                            StudentEntity.class,
                                            Integer.parseInt(queryResultsTable.getValueAt(chosen_row_index, 0).toString())
                                    );
                                    em.remove(chosen_student);
                                    em.getTransaction().commit();
                                    tableModelUpdater.updateStudent();
                                    break;
                            }
                            queryResultsTable.setModel(table_model);
                        }
                        catch (ArrayIndexOutOfBoundsException noRowSelectedError) {
                            //выбрасываю окошко с предупреждением: выберите ряд для удаления
                            JOptionPane.showMessageDialog(contentPane, "Choose row", "Error", JOptionPane.ERROR_MESSAGE);
                        }
                        clear_GUI();
                    }
                }
        );

        clearSelectButton.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        clear_GUI();
                    }
                }
        );

        applyChangesButton.addActionListener(
                new ActionListener() {
                    @Override
                    /**
                     * если сущность выбрана - меняет ее, иначе - добавляет новую в таблицу
                     */
                    public void actionPerformed(ActionEvent e) {
                        int chosen_row_index = queryResultsTable.getSelectedRow();

                        EntityManager em = emf.createEntityManager();
                        em.getTransaction().begin();

                        if (chosen_row_index != -1) {
                            //если что-то выбрано, то сущность надо менять
                            int chosen_entity_index = classesTabbedPane.getSelectedIndex();
                            String chosen_entity_name = classesTabbedPane.getTitleAt(chosen_entity_index);

                            switch (chosen_entity_name) {
                                case "class":

                                    ClazzEntity chosen_class = em.find(
                                            ClazzEntity.class,
                                            Integer.parseInt(queryResultsTable.getValueAt(chosen_row_index, 0).toString())
                                    );

                                    int class_id = chosen_class.getClassId();
                                    try {
                                        int class_number = Integer.parseInt(classNumberTextField.getText());
                                        String class_letter = classLetterTextField.getText();

                                        entityUpdater.updateClass(class_id, class_number, class_letter);
                                        tableModelUpdater.updateClass();
                                    }
                                    catch (WrongInputException wrongInput) {
                                        JOptionPane.showMessageDialog(contentPane, wrongInput.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                                    }
                                    catch (NumberFormatException NaN) {
                                        JOptionPane.showMessageDialog(contentPane, "Enter valid class", "Error", JOptionPane.ERROR_MESSAGE);
                                    }
                                    break;

                                case "teacher":
                                    TeacherEntity chosen_teacher = em.find(
                                            TeacherEntity.class,
                                            Integer.parseInt(queryResultsTable.getValueAt(chosen_row_index, 0).toString())
                                    );
                                    int teacher_id = chosen_teacher.getTeacherId();

                                    String teacher_firstname = teacherFirstnameTextField.getText();
                                    String teacher_surname = teacherSurnameTextField.getText();
                                    String teacher_patronymic = teacherPatronymicTextField.getText();
                                    String teacher_comment = teacherCommentTextField.getText();

                                    try {
                                        entityUpdater.updateTeacher(teacher_id, teacher_surname, teacher_firstname, teacher_patronymic, teacher_comment);
                                        tableModelUpdater.updateTeacher();
                                    }
                                    catch (WrongInputException wrongInput) {
                                        JOptionPane.showMessageDialog(contentPane, wrongInput.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                                    }

                                    break;

                                case "student":
                                    StudentEntity chosen_student = em.find(
                                            StudentEntity.class,
                                            Integer.parseInt(queryResultsTable.getValueAt(chosen_row_index, 0).toString())
                                    );
                                    int student_id = chosen_student.getStudentId();

                                    String student_firstname = studentFirstnameTextField.getText();
                                    String student_surname = studentSurnameTextField.getText();
                                    String student_patronymic = studentPatronymicTextField.getText();
                                    String student_comment = studentCommentTextField.getText();

                                    try {
                                        int student_class_number = Integer.parseInt(studentClassNumberTextField.getText());
                                        String student_class_letter = studentClassLetterTextField.getText();

                                        entityUpdater.updateStudent(student_id, student_class_number, student_class_letter, student_surname, student_firstname, student_patronymic, student_comment);
                                        tableModelUpdater.updateStudent();
                                    }
                                    catch (WrongInputException wrongInput) {
                                        JOptionPane.showMessageDialog(contentPane, wrongInput.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);

                                    }
                                    catch (NumberFormatException NaN) {
                                        JOptionPane.showMessageDialog(contentPane, "Enter valid class", "Error", JOptionPane.ERROR_MESSAGE);

                                    }

                            }
                        }
                        else {
                            //in this branch program is adding instances to database:
                            int chosen_entity_index = classesTabbedPane.getSelectedIndex();
                            String chosen_entity_name = classesTabbedPane.getTitleAt(chosen_entity_index);

                            switch (chosen_entity_name) {
                                case "class":
                                    try {
                                        int class_number = Integer.parseInt(classNumberTextField.getText());
                                        String class_letter = classLetterTextField.getText();

                                        entityAdder.addClass(class_number, class_letter);
                                    }
                                    catch (WrongInputException wrongInput) {
                                        JOptionPane.showMessageDialog(contentPane, wrongInput.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                                    }
                                    catch (NumberFormatException NaN) {
                                        JOptionPane.showMessageDialog(contentPane, "Enter valid class", "Error", JOptionPane.ERROR_MESSAGE);
                                    }
                                     tableModelUpdater.updateClass();

                                    break;
                                case "teacher":

                                    String teacher_firstname = teacherFirstnameTextField.getText();
                                    String teacher_surname = teacherSurnameTextField.getText();
                                    String teacher_patronymic = teacherPatronymicTextField.getText();
                                    String teacher_comment = teacherCommentTextField.getText();

                                    try {
                                        entityAdder.addTeacher(teacher_surname, teacher_firstname, teacher_patronymic, teacher_comment);
                                        tableModelUpdater.updateTeacher();
                                    }
                                    catch (WrongInputException wrongInput) {
                                        JOptionPane.showMessageDialog(contentPane, wrongInput.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                                    }

                                    break;
                                case "student":

                                    String student_firstname = studentFirstnameTextField.getText();
                                    String student_surname = studentSurnameTextField.getText();
                                    String student_patronymic = studentPatronymicTextField.getText();
                                    String student_comment = studentCommentTextField.getText();

                                    try {
                                        int student_class_number = Integer.parseInt(studentClassNumberTextField.getText());
                                        String student_class_letter = studentClassLetterTextField.getText();

                                        entityAdder.addStudent(student_class_number, student_class_letter, student_surname, student_firstname, student_patronymic, student_comment);
                                        tableModelUpdater.updateStudent();
                                    }
                                    catch (WrongInputException wrongInput) {
                                        JOptionPane.showMessageDialog(contentPane, wrongInput.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);

                                    }
                                    catch (NumberFormatException NaN) {
                                        JOptionPane.showMessageDialog(contentPane, "Enter valid class", "Error", JOptionPane.ERROR_MESSAGE);

                                    }
                                    break;
                            }
                        }

                        //renew table model, clear GUI inputs
                        queryResultsTable.setModel(table_model);
                        clear_GUI();
                    }

                }
        );

        fGradersButton.addActionListener(
                new ActionListener() {
                    @Override
                    /**
                     * выводит на экран таблицу с двоечниками
                     */
                    public void actionPerformed(ActionEvent e) {
                        tableModelUpdater.updateFGraders();
                        queryResultsTable.setModel(table_model);
                    }
                }
        );

        aGradersButton.addActionListener(
                new ActionListener() {
                    @Override
                    /**
                     * выводит на экран  таблицу с отличниками
                     */
                    public void actionPerformed(ActionEvent e) {
                        tableModelUpdater.updateAGraders();
                        queryResultsTable.setModel(table_model);
                    }
                }
        );

        hostsButton.addActionListener(
                new ActionListener() {
                    @Override
                    /**
                     * если учитель выделен, то выводит данные о нем
                     * @see TableModelUpdater#updateHost(int)
                     * иначе выводит данные о всех учителях
                     * @see TableModelUpdater#updateHostAll()
                     */
                    public void actionPerformed(ActionEvent e) {

                        EntityManager em = emf.createEntityManager();
                        int chosen_row_index = queryResultsTable.getSelectedRow();

                        if (chosen_row_index != -1) {
                            TeacherEntity chosen_teacher = em.find(
                                    TeacherEntity.class,
                                    Integer.parseInt(queryResultsTable.getValueAt(chosen_row_index, 0).toString())
                            );
                            int teacher_id = chosen_teacher.getTeacherId();
                            tableModelUpdater.updateHost(teacher_id);
                        }
                        else {
                            tableModelUpdater.updateHostAll();
                        }

                        queryResultsTable.setModel(table_model);
                    }
                }
        );

        teachesSubjectsButton.addActionListener(
                new ActionListener() {
                    @Override
                    /**
                     * если учитель выделен, то выводит данные о нем
                     * @see TableModelUpdater#updateTeachesSubject(int)
                     * иначе выводит данные о всех учителях
                     * @see TableModelUpdater#updateTeachesSubjectAll()
                     */
                    public void actionPerformed(ActionEvent e) {
                        EntityManager em = emf.createEntityManager();
                        int chosen_row_index = queryResultsTable.getSelectedRow();

                        if (chosen_row_index != -1) {
                            TeacherEntity chosen_teacher = em.find(
                                    TeacherEntity.class,
                                    Integer.parseInt(queryResultsTable.getValueAt(chosen_row_index, 0).toString())
                            );
                            int teacher_id = chosen_teacher.getTeacherId();
                            tableModelUpdater.updateTeachesSubject(teacher_id);
                        }
                        else {
                            tableModelUpdater.updateTeachesSubjectAll();
                        }

                        queryResultsTable.setModel(table_model);
                    }
                }
        );

        exportFGradersButton.addActionListener(
                new ActionListener() {
                    @Override
                    /**
                     * создает отчет по количеству двоечников
                     */
                    public void actionPerformed(ActionEvent e) {
                        tableModelUpdater.updateFGraders();
                        queryResultsTable.setModel(table_model);
                        try {
                            PDFCreator.createSamplePDF("reports/FGraders.pdf", new String[]{"id", "surname", "firstname", "class", "avg"}, table_model);
                        }
                        catch (Exception exception) {
                            JOptionPane.showMessageDialog(contentPane, "Cant export: " + exception.getClass() + ':' + exception.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }
        );

        exportAGradersButton.addActionListener(
                new ActionListener() {
                    @Override
                    /**
                     * создает отчет по количеству отличников
                     */
                    public void actionPerformed(ActionEvent e) {
                        tableModelUpdater.updateAGraders();
                        queryResultsTable.setModel(table_model);
                        try {
                            PDFCreator.createSamplePDF("reports/AGraders.pdf", new String[]{"id", "surname", "firstname", "class", "avg"}, table_model);
                        }
                        catch (Exception exception) {
                            JOptionPane.showMessageDialog(contentPane, "Cant export: " + exception.getClass() + ':' + exception.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }
        );

        exportStudentsCountButton.addActionListener(
                new ActionListener() {
                    @Override
                    /**
                     * создает отчет по количеству студентов по классам
                     */
                    public void actionPerformed(ActionEvent e) {
                        // сформируем table model, но пихать ее никуда не будем
                        DefaultTableModel temp = tableModelUpdater.createReportTableForStudentsCount();
                        try {
                            PDFCreator.createSamplePDF("reports/students.pdf", new String[]{"id", "class", "students count"}, temp);
                        }
                        catch (Exception exception) {
                            JOptionPane.showMessageDialog(contentPane, "Cant export: " + exception.getClass() + ':' + exception.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }
        );
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        app_GUI dialog = new app_GUI();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
