package school_database_classes;

import javax.persistence.*;

@Entity
@Table(name = "class", schema = "school", catalog = "")
public class ClazzEntity {
    private int classId;
    private int classNumber;
    private String classLetter;

    @Id
    @Column(name = "class_id")
    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    @Basic
    @Column(name = "class_number")
    public int getClassNumber() {
        return classNumber;
    }

    public void setClassNumber(int classNumber) {
        this.classNumber = classNumber;
    }

    @Basic
    @Column(name = "class_letter")
    public String getClassLetter() {
        return classLetter;
    }

    public void setClassLetter(String classLetter) {
        this.classLetter = classLetter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClazzEntity that = (ClazzEntity) o;

        if (classId != that.classId) return false;
        if (classNumber != that.classNumber) return false;
        if (classLetter != null ? !classLetter.equals(that.classLetter) : that.classLetter != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = classId;
        result = 31 * result + classNumber;
        result = 31 * result + (classLetter != null ? classLetter.hashCode() : 0);
        return result;
    }
}
