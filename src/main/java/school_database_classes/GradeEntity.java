package school_database_classes;

import javax.persistence.*;

@Entity
@Table(name = "grade", schema = "school", catalog = "")
public class GradeEntity {
    private int gradeId;
    private int value;
    private StudentEntity studentByStudentId;
    private TeacherEntity teacherByTeacherId;


    @Id
    @Column(name = "grade_id")
    public int getGradeId() {
        return gradeId;
    }

    public void setGradeId(int gradeId) {
        this.gradeId = gradeId;
    }

    @Basic
    @Column(name = "value")
    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GradeEntity that = (GradeEntity) o;

        if (gradeId != that.gradeId) return false;
        if (value != that.value) return false;

        return true;
    }

    @ManyToOne
    @JoinColumn(name = "student_id", referencedColumnName = "student_id")
    public StudentEntity getStudentByStudentId() {
        return studentByStudentId;
    }

    public void setStudentByStudentId(StudentEntity studentByStudentId) {
        this.studentByStudentId = studentByStudentId;
    }

    @Override
    public int hashCode() {
        int result = gradeId;
        result = 31 * result + value;
        return result;
    }
}
