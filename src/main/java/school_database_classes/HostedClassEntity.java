package school_database_classes;

import javax.persistence.*;

@Entity
@Table(name = "hosted_class", schema = "school", catalog = "")
public class HostedClassEntity {
    private int hostedClassId;
    private ClazzEntity clazzByClassId;
    private TeacherEntity teacherByTeacherId;

    @Id
    @Column(name = "hosted_class_id")
    public int getHostedClassId() {
        return hostedClassId;
    }

    public void setHostedClassId(int hostedClassId) {
        this.hostedClassId = hostedClassId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HostedClassEntity that = (HostedClassEntity) o;

        if (hostedClassId != that.hostedClassId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return hostedClassId;
    }

    @ManyToOne
    @JoinColumn(name = "class_id", referencedColumnName = "class_id")
    public ClazzEntity getClazzByClassId() {
        return clazzByClassId;
    }

    public void setClazzByClassId(ClazzEntity clazzByClassId) {
        this.clazzByClassId = clazzByClassId;
    }

    @ManyToOne
    @JoinColumn(name = "teacher_id", referencedColumnName = "teacher_id")
    public TeacherEntity getTeacherByTeacherId() {
        return teacherByTeacherId;
    }

    public void setTeacherByTeacherId(TeacherEntity teacherByTeacherId) {
        this.teacherByTeacherId = teacherByTeacherId;
    }
}
