package logic;

import errors.WrongInputException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * тестирование EntityAdder на корректность
 */
public class EntityAdderTest {

    boolean stringFormatClassLetterMustThrowError() {
        EntityAdder test = new EntityAdder();
        try {
            test.addClass('1', "sda");
        }
        catch(WrongInputException w ){
                return true;
        }
        System.out.println("test 3 failed");
        return false;
    }

    boolean emptyTeacherNameOrSurnameMustThrowError() {
        EntityAdder test = new EntityAdder();
        try {
            test.addTeacher("", "", "", "");
        }
        catch( WrongInputException w ) {
            return true;
        }
        System.out.println("test 3 failed");
        return false;
    }

    boolean emptyStudentNameOrSurnameMustThrowError() {
        EntityAdder test = new EntityAdder();
        try {
            //it is guaranteed, that class 1 B exists
            test.addStudent(1, "B", "", "", "", "");
        }
        catch( WrongInputException w ) {
            return true;
        }
        System.out.println("test 3 failed");
        return false;
    }

    @Test
    public void testClass() {
        assertEquals(true, stringFormatClassLetterMustThrowError());
    }

    @Test
    public void testTeacher() {
        assertEquals(true, emptyTeacherNameOrSurnameMustThrowError());
    }

    @Test
    public void testStudent() {
        assertEquals(true, emptyStudentNameOrSurnameMustThrowError());
    }

}